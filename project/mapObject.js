function mapObject(obj, cb) {
    const result = {};
    for (const key in obj) {

        // It checks if the property belongs to the object itself 
        if (obj.hasOwnProperty(key)) {
            result[key] = cb(obj[key]);
        }
    }
    return result;
}

module.exports = mapObject;