function keys(obj) {
    if (typeof obj !== 'object' || obj === null) {
        throw new TypeError('Argument should be an object');
    }

    return Object.keys(obj);
}

module.exports = keys;