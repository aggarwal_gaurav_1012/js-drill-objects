function invert(obj) {
    const invertedObj = {};
    for (const key in obj) {
        // it ensures that the property being iterated over 'key' actually belongs to the object 'obj'.
        if (Object.hasOwnProperty.call(obj, key)) {
            const value = obj[key];
            invertedObj[value] = key;
        }
    }
    return invertedObj;
}

module.exports = invert;