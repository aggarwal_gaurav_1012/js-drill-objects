function pairs(obj) {

    if (typeof obj !== 'object' || obj === null) {
        throw new TypeError('Input should be an object');
    }
    return Object.entries(obj);
}

module.exports = pairs;