const keys = require('../project/keys');

// Test case
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

try {
    console.log(keys(testObject));
} catch (error) {
    console.error('Error:', error.message);
}