const assert = require('assert');
const values = require('../project/values');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// Test the values function
const result = values(testObject);
assert.deepStrictEqual(result, ['Bruce Wayne', 36, 'Gotham']);

// If the assertion passes, no error will be thrown
console.log(result);