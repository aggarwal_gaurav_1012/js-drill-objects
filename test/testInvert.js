const invert = require('../project/invert');

// Test cases
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log("Inverted Object:", invert(testObject));