const mapObject = require('../project/mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function doubleAge(age) {
    return age * 2;
}

console.log(mapObject(testObject, doubleAge));